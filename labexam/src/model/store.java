package model;

public class store {
         private String Name;
         private double Price;
         private float Quantity;
         
	public store() {
		// TODO Auto-generated constructor stub
	}

	public store(String name, double price, float quantity) {
		super();
		Name = name;
		Price = price;
		Quantity = quantity;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	public float getQuantity() {
		return Quantity;
	}

	public void setQuantity(float quantity) {
		Quantity = quantity;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toString() {
		return "store [Name=" + Name + ", Price=" + Price + ", Quantity=" + Quantity + "]";
	}

}
